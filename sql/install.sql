CREATE DATABASE IF NOT EXISTS restbird ;
USE restbird;

CREATE TABLE tbl_student (
  id bigint(10) NOT NULL AUTO_INCREMENT,
  sid varchar(32) DEFAULT NULL,
  sname varchar(32) DEFAULT NULL,
  sage tinyint(4) DEFAULT NULL,
  ssex varchar(2) DEFAULT NULL,
  sclass varchar(32) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY sid_UNIQUE (sid)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


INSERT INTO tbl_student VALUES (1, '15075501', 'tom', 18, '1', '121322');
INSERT INTO tbl_student VALUES (2, '15075603', 'jack', 19, '1', '121325');
INSERT INTO tbl_student VALUES (3, '15075502', 'lucy', 17, '2', '121322');
INSERT INTO tbl_student VALUES (4, '15075605', 'jim', 18, '1', '121325');
INSERT INTO tbl_student VALUES (5, '15075707', 'seven', 18, '2', '121327');
